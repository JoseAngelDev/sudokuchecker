using SudokuLogic;

namespace TestSudokuProject
{
    [TestClass]
    public class UnitTestSudokuChecker
    {
        private readonly int[,] _boardTrue = 

                {{ 7, 9, 2, 1, 5, 4, 3, 8, 6 },
                { 6, 4, 3, 8, 2, 7, 1, 5, 9 },
                { 8, 5, 1, 3, 9, 6, 7, 2, 4 },
                { 2, 6, 5, 9, 7, 3, 8, 4, 1 },
                { 4, 8, 9, 5, 6, 1, 2, 7, 3 },
                { 3, 1, 7, 4, 8, 2, 9, 6, 5 },
                { 1, 3, 6, 7, 4, 8, 5, 9, 2 },
                { 9, 7, 4, 2, 1, 5, 6, 3, 8 },
                { 5, 2, 8, 6, 3, 9, 4, 1, 7 } };
        
        private readonly int[,] _boardFalse =

               {{ 7, 9, 2, 1, 5, 4, 3, 8, 6 },
                { 6, 4, 3, 8, 2, 7, 1, 5, 9 },
                { 8, 5, 1, 3, 9, 6, 7, 2, 4 },
                { 2, 6, 5, 9, 7, 3, 8, 4, 1 },
                { 4, 8, 9, 5, 6, 1, 2, 7, 3 },
                { 3, 1, 7, 4, 8, 2, 9, 6, 5 },
                { 1, 3, 6, 7, 4, 8, 5, 9, 2 },
                { 9, 7, 4, 2, 1, 5, 6, 3, 8 },
                { 5, 2, 8, 6, 3, 9, 4, 1, 1 } };

        [TestMethod]
        public void IsInRange()
        {
            Assert.AreEqual(expected:true, actual:SudokuValidator.IsInRange(_boardTrue));
        }

        [TestMethod]
        public void IsSudokuInvalid()
        {
            Assert.AreEqual(expected:false, actual:SudokuValidator.IsSudokuSolved(_boardFalse));
        }

        [TestMethod]
        public void IsSudokuValid()
        {
            Assert.AreEqual(expected:true, actual:SudokuValidator.IsSudokuSolved(_boardTrue));
        }

    }
}