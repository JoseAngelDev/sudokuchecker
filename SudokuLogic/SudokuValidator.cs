﻿namespace SudokuLogic
{
    public static class SudokuValidator
    {
        const int N = 9;
        /// <summary>
        /// Verify that the values in the matrix are between 1 and 9.
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public static bool IsInRange(int[,] board)
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (board[i, j] < 1 || board[i, j] > 9)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Verify if the sudoku is solved.
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public static bool IsSudokuSolved(int[,] board)
        {
            if (IsInRange(board) == false)
            {
                return false;
            }
            for (int i = 0; i < 9; i++)
            {
                var rowTable = new HashSet<int>();
                var columnTable = new HashSet<int>();

                for (int j = 0; j < 9; j++)
                {
                    int columnValue = board[i, j];
                    int rowValue = board[j, i];

                    if (columnTable.Contains(columnValue))
                    {
                        return false;
                    }
                    columnTable.Add(columnValue);

                    if (rowTable.Contains(rowValue))
                    {
                        return false;
                    }
                    rowTable.Add(rowValue);
                }
            }
            return true;
        }
    }

}